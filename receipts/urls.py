from django.urls import path
from receipts.views import (
    receipt_list,
    receipt_create,
    expense_list,
    account_list,
    expense_create,
    account_create,
)


urlpatterns = [
    path("", receipt_list, name="home"),
    path("create/", receipt_create, name="create_receipt"),
    path("categories/", expense_list, name="category_list"),
    path("accounts/", account_list, name="account_list"),
    path("categories/create/", expense_create, name="create_category"),
    path("accounts/create/", account_create, name="create_account"),
]
