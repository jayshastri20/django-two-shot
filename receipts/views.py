from django.shortcuts import render, redirect
from receipts.models import ExpenseCategory, Account, Receipt
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm

# Create your views here.


@login_required(redirect_field_name="registration/login/")
def receipt_list(request):
    receipts_list = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts_list": receipts_list,
    }
    return render(request, "receipts/list.html", context)


def redirect_receipt_list(request):
    response = redirect("home")
    return response


@login_required(redirect_field_name="registration/login/")
def receipt_create(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required(redirect_field_name="registration/login/")
def expense_list(request):
    expenses_list = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "expenses_list": expenses_list,
    }
    return render(request, "categories/list.html", context)


@login_required(redirect_field_name="registration/login/")
def expense_create(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expense = form.save(False)
            expense.owner = request.user
            expense.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()

    context = {
        "form": form,
    }
    return render(request, "categories/create.html", context)


@login_required(redirect_field_name="registration/login/")
def account_list(request):
    accounts_list = Account.objects.filter(owner=request.user)
    context = {
        "accounts_list": accounts_list,
    }
    return render(request, "accounts/list.html", context)


@login_required(redirect_field_name="registration/login/")
def account_create(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()

    context = {
        "form": form,
    }
    return render(request, "accounts/create.html", context)
